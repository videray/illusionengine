LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libIllusion
LOCAL_VENDOR_MODULE := true
LOCAL_SHARED_LIBRARIES := \
  liblog \
  libcutils \
  libutils \
  libOpenCL \
  libvideray_core

LOCAL_EXPORT_C_INCLUDE_DIRS := \
  $(LOCAL_PATH)/include

LOCAL_C_INCLUDES := \
  $(LOCAL_PATH)/include \
  $(LOCAL_PATH)/opencl \
	$(FSL_PROPRIETARY_PATH)/fsl-proprietary/include

LOCAL_SRC_FILES := \
  src/SharedBuffer.cpp \
  src/Color.cpp \
  src/PseudoColorLookupTable.cpp \
  src/Image2D.cpp \
  src/IllusionEngine.cpp \
  src/ImagePipeline.cpp

#LOCAL_CFLAGS += -g -O0

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := illusion_tests
LOCAL_MODULE_TAGS := tests
LOCAL_SHARED_LIBRARIES := \
  liblog \
  libcutils \
  libutils \
  libOpenCL \
  libvideray_core \
  libIllusion

LOCAL_SRC_FILES := \
  test/main.cpp \
  test/illusionengine_test.cpp \
  test/opencl_stages.cpp

LOCAL_C_INCLUDES := \
  $(FSL_PROPRIETARY_PATH)/fsl-proprietary/include \
  $(LOCAL_PATH)/opencl

#LOCAL_CFLAGS += -g -O0

include $(BUILD_NATIVE_TEST)