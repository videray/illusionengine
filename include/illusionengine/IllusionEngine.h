#ifndef IE_ILLUSIONENGINE_H
#define IE_ILLUSIONENGINE_H

#include <memory>

#include <utils/Errors.h>

#include <videray/core.h>

namespace videray {

using ::android::status_t;

template<typename T>
using up = std::unique_ptr<T>;

struct Dim1D {
  Dim1D();
  Dim1D( size_t w );

  union {
    size_t width;
    size_t x;
    size_t len;
  };

};

struct Dim2D {
  Dim2D();
  Dim2D( size_t w, size_t h );

  union {
    size_t width;
    size_t x;
  };

  union {
    size_t height;
    size_t y;
  };
};

class Stage;
class EqualizeHistoStage;

class IllusionEngine
{
public:
  static IllusionEngine& getInstance();

  VIDERAY_NO_COPY(IllusionEngine)

  up<Stage> stage_normalize();

  up<EqualizeHistoStage> stage_equalize();
  up<EqualizeHistoStage> stage_raw_equalize();

private:
  IllusionEngine();
  ~IllusionEngine();

  struct IE_Data;
  IE_Data* mData;
};

} // namespace videray

#endif // IE_ILLUSIONENGINE_H