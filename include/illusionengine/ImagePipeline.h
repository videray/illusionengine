#ifndef IE_IMAGE_PIPELINE_H
#define IE_IMAGE_PIPELINE_H

#include <illusionengine/IllusionEngine.h>
#include <illusionengine/Image2D.h>

#include <vector>

namespace videray {

class Stage
{
public:
  virtual ~Stage() {}
  virtual status_t run( Image2D& input, Image2D& output ) = 0;

};

class EqualizeHistoStage : public Stage
{
public:
  std::vector<float> cdf; // normalized cumulative distribution function
};

class ImagePipeline
{
public:

  inline
  //std::vector< ptr<Stage>>& stages();

  status_t run( Image2D& input, Image2D& output );

private:
  //std::vector< ptr<Stage>> mStages;
};

///////// implementation ///////

//std::vector< ptr<Stage>>& ImagePipeline::stages()
//{
//  return mStages;
//}


} // namespace videray

#endif // IE_IMAGE_PIPELINE_H