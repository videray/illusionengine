#ifndef VIDERAY_PSEUDOCOLORLOOKUPTABLE_H
#define VIDERAY_PSEUDOCOLORLOOKUPTABLE_H


#include <illusionengine/Image2D.h>
#include <illusionengine/Color.h>

#include <vector>

namespace videray {


class PseudoColorLookupTable
{
public:

  static
  void createGrayLookupTable( int resolution, Image1D& );

  static
  void createSpectrumLookupTable( int resolution, Image1D& );

  static
  void createColorBlendLUT( int resolution, std::vector<Color> colors, Image1D& lut );

  static
  void createThermalLUT( int resolution, Image1D& lut );

  static
  void createRedHotLUT( int resolution, Image1D& );

  static
  void createSRangeLUT( int resolution, Image1D& );

  static
  void create8Color( int resolution, Image1D& );

  static
  void create16Color( int resolution, Image1D& );

  static
  void createSin( int resolution, Image1D&, float frequency, float r, float g, float b );

};

}

#endif // VIDERAY_PSEUDOCOLORLOOKUPTABLE_H