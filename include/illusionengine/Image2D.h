#ifndef IE_IMAGE_H
#define IE_IMAGE_H

#include <illusionengine/IllusionEngine.h>

namespace videray {

struct ImageBufferInfo {

  enum {
    PIX_FMT_RGBA8888 = 1,
    PIX_FMT_RGB565,
    PIX_FMT_GRAY_UINT8,   // unsigned int8
    PIX_FMT_GRAY_SINT8,
    PIX_FMT_GRAY_UINT16,  // unsigned int16
    PIX_FMT_GRAY_SINT16,
    PIX_FMT_GRAY_FLOAT,   // normalized float-32
  };

  uint8_t format;

};

struct ImageBufferInfo1D : public ImageBufferInfo {
  Dim1D size;
};

struct ImageBufferInfo2D : public ImageBufferInfo {
  Dim2D size;

  /**
  * the scan-line pitch in bytes. This can either be 0 or >= image_width * size_of_pixel_in_bytes.
  * If row_pitch = 0, then row_pitch is interpreted as image_width * size_of_pixel_in_bytes.
  * If row_pitch is not 0, it must be a multiple of the image element size in bytes.
  */
  size_t row_pitch;
};

class Image1D
{
public:
  Image1D();
  Image1D( uint8_t format, const Dim1D& );
  Image1D( const Image1D& );
  ~Image1D();

  const void* data() const;
  void* data();

  inline
  const ImageBufferInfo1D& info() const;

  Image1D& operator=( const Image1D& );

private:
  ImageBufferInfo1D mInfo;
  void* mBuffer;
};

class Image2D
{
public:
  Image2D();
  Image2D( uint8_t format, const Dim2D& );
  Image2D( const Image2D& );
  ~Image2D();

  const void* data() const;
  void* data();

  inline
  const ImageBufferInfo2D& info() const;

  Image2D& operator=( const Image2D& );

private:
  ImageBufferInfo2D mInfo;
  void* mBuffer;
};


//////// Implementation /////////////

const ImageBufferInfo1D& Image1D::info() const
{
  return mInfo;
}

const ImageBufferInfo2D& Image2D::info() const
{
  return mInfo;
}

} // namespace videray

#endif // IE_IMAGE_H