#ifndef VIDERAY_COLOR_H_
#define VIDERAY_COLOR_H_

#include <stdint.h>

namespace videray {

struct Color {
  float r, g, b, a;

  static
  Color hexToColor( const char* );

  static
  void HSVtoRGB( float& fR, float& fG, float& fB, const float& fH, const float& fS, const float& fV );

  static
  void RGBtoHSV( const float& fR, const float& fG, const float& fB, float& fH, float& fS, float& fV );

  static
  Color mix( const Color& a, const Color& b, float t );
};

class RGB565
{
public:
  static Color toColor( uint16_t );
  static uint16_t fromColor( Color );

};

}

#endif // VIDERAY_COLOR_H_