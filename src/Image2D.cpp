#include <illusionengine/IllusionEngine.h>
#include <illusionengine/Image2D.h>

#include "SharedBuffer.h"

namespace videray {

static
size_t getBytePerPoint( uint8_t format )
{
  switch( format ) {

  case ImageBufferInfo::PIX_FMT_RGBA8888:
  case ImageBufferInfo::PIX_FMT_GRAY_FLOAT:
    return 4;

  case ImageBufferInfo::PIX_FMT_RGB565:
  case ImageBufferInfo::PIX_FMT_GRAY_UINT16:
  case ImageBufferInfo::PIX_FMT_GRAY_SINT16:
    return 2;

  case ImageBufferInfo::PIX_FMT_GRAY_SINT8:
  case ImageBufferInfo::PIX_FMT_GRAY_UINT8:
    return 1;

  //unknown format
  default:
    return 0;
  }

}

static
size_t getPixelBufferSize( const ImageBufferInfo1D& info )
{
  size_t numPoints = info.size.x;
  return numPoints * getBytePerPoint( info.format );
}

static
size_t getPixelBufferSize( const ImageBufferInfo2D& info )
{
  size_t numPoints = info.size.width * info.size.height;
  return numPoints * getBytePerPoint( info.format );
}


/////////////////////////////////////////

Image1D::Image1D()
  : mBuffer( nullptr )
{
  mInfo.format = 0;
}

Image1D::Image1D( uint8_t format, const Dim1D& size )
{
  mInfo.format = format;
  mInfo.size = size;

  size_t bufSize = getPixelBufferSize( mInfo );
  mBuffer = SharedBuffer::alloc( bufSize )->data();
}

Image1D::Image1D( const Image1D& copy )
  : mInfo( copy.mInfo )
{
  if( copy.mBuffer != nullptr ) {
    SharedBuffer::bufferFromData( copy.mBuffer )->acquire();
  }

  mBuffer = copy.mBuffer;
}

Image1D::~Image1D()
{
  if( mBuffer != nullptr ) {
    SharedBuffer::bufferFromData( mBuffer )->release();
  }
  mBuffer = nullptr;
}

const void* Image1D::data() const
{
  return mBuffer;
}

void* Image1D::data()
{
  return mBuffer;
}

Image1D& Image1D::operator=( const Image1D& rhs )
{
  if( rhs.mBuffer != nullptr ) {
    SharedBuffer::bufferFromData( rhs.mBuffer )->acquire();
  }
  if( mBuffer != nullptr ) {
    SharedBuffer::bufferFromData( mBuffer )->release();
  }

  mBuffer = rhs.mBuffer;
  mInfo = rhs.mInfo;
  return *this;
}

/////////////////////////////////////

Image2D::Image2D()
  : mBuffer( nullptr )
{
  mInfo.format = 0;
  mInfo.row_pitch = 0;
}

Image2D::Image2D( uint8_t format, const Dim2D& size )
{
  mInfo.format = format;
  mInfo.size = size;
  mInfo.row_pitch = 0;

  size_t bufSize = getPixelBufferSize( mInfo );
  mBuffer = SharedBuffer::alloc( bufSize )->data();
}

Image2D::Image2D( const Image2D& copy )
  : mInfo( copy.mInfo )
{
  if( copy.mBuffer != nullptr ) {
    SharedBuffer::bufferFromData( copy.mBuffer )->acquire();
  }

  mBuffer = copy.mBuffer;
}

Image2D::~Image2D()
{
  if( mBuffer != nullptr ) {
    SharedBuffer::bufferFromData( mBuffer )->release();
  }
  mBuffer = nullptr;
}

const void* Image2D::data() const
{
  return mBuffer;
}

void* Image2D::data()
{
  return mBuffer;
}

Image2D& Image2D::operator=( const Image2D& rhs )
{
  if( rhs.mBuffer != nullptr ) {
    SharedBuffer::bufferFromData( rhs.mBuffer )->acquire();
  }
  if( mBuffer != nullptr ) {
    SharedBuffer::bufferFromData( mBuffer )->release();
  }

  mBuffer = rhs.mBuffer;
  mInfo = rhs.mInfo;
  return *this;
}




} // namespace videray