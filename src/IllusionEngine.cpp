
#define LOG_TAG "illusion"
#include <log/log.h>

#include <utils/Mutex.h>


#include <illusionengine/IllusionEngine.h>
#include <illusionengine/ImagePipeline.h>

#define CL_HPP_MINIMUM_OPENCL_VERSION 100
#define CL_HPP_TARGET_OPENCL_VERSION 120
#include <cl2.hpp>

const std::string kernel =
#include "equalize.cl"
  ;

using namespace android;



namespace videray {


Dim1D::Dim1D()
  : width( 0 )
{}

Dim1D::Dim1D( size_t w )
  : width( w )
{}

Dim2D::Dim2D()
  : width( 0 ), height( 0 )
{}

Dim2D::Dim2D( size_t w, size_t h )
  : width( w ), height( h )
{}


Mutex sLock;
IllusionEngine* sInstance( nullptr );

IllusionEngine& IllusionEngine::getInstance()
{
  Mutex::Autolock lock( sLock );
  if( sInstance == nullptr ) {
    sInstance = new IllusionEngine();
  }
  return *sInstance;
}

struct IllusionEngine::IE_Data {
  cl::Platform platform;
  cl::Device device;
  cl::Context context;
  cl::CommandQueue queue;
  cl::Program program;

  status_t initOpenCL();
};

status_t IllusionEngine::IE_Data::initOpenCL()
{
  cl_int err;

  std::vector<cl::Platform> platforms;
  cl::Platform::get( &platforms );
  if( platforms.empty() ) {
    ALOGE( "no OpenCL platforms" );
    return UNKNOWN_ERROR;
  }

  platform = cl::Platform::setDefault( platforms.front() );

  std::vector<cl::Device> devices;
  platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );
  if( devices.empty() ) {
    ALOGE( "no OpenCL devices" );
    return UNKNOWN_ERROR;
  }

  device = cl::Device::setDefault( devices.front() );
  context = cl::Context::setDefault( cl::Context( device ) );
  queue = cl::CommandQueue::setDefault( cl::CommandQueue( context, device ) );


  program = cl::Program( kernel );
  err = program.build( "-cl-std=CL1.2" );
  if( err != 0 ) {
    std::string buildlog = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>().front().second;
    ALOGE( "error compiling opencl: %s", buildlog.c_str() );
    return UNKNOWN_ERROR;
  }

  return NO_ERROR;
}


IllusionEngine::IllusionEngine()
{
  mData = new IE_Data();
  if( mData->initOpenCL() != NO_ERROR ) {
    ALOGE( "error init opencl" );
  }
}

IllusionEngine::~IllusionEngine()
{
  delete mData;
}


class NormalizeOpenCLStage : public Stage
{
public:

  cl::Context context;
  cl::Kernel kernel;
  cl::CommandQueue queue;

  status_t run( Image2D& input, Image2D& output ) {

    if( input.info().format != ImageBufferInfo::PIX_FMT_GRAY_UINT16 ) {
      ALOGE( "input format is not correct: %d", input.info().format );
      return UNKNOWN_ERROR;
    }

    if( output.info().format != ImageBufferInfo::PIX_FMT_GRAY_FLOAT ) {
      ALOGE( "output format is not correct: %d", output.info().format );
      return UNKNOWN_ERROR;
    }

    cl_int err;
    const Dim2D& inputSize = input.info().size;
    const Dim2D& outputSize = output.info().size;

    cl::Image2D inputImage( context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                            cl::ImageFormat( CL_R, CL_SIGNED_INT16 ), inputSize.width, inputSize.height, 0, input.data(), &err );

    if( err != 0 ) {
      ALOGE( "error creating input buffer: %d", err );
      return UNKNOWN_ERROR;
    }

    cl::Image2D outputImage( context, CL_MEM_WRITE_ONLY,
                             cl::ImageFormat( CL_R, CL_FLOAT ), outputSize.width, outputSize.height, 0 );

    const std::array<std::size_t, 3> origin = {0, 0, 0};
    const std::array<std::size_t, 3> region = {inputSize.width, inputSize.height, 1};


    kernel.setArg( 0, inputImage );
    kernel.setArg( 1, outputImage );
    queue.enqueueNDRangeKernel( kernel, cl::NullRange, cl::NDRange( inputSize.width, inputSize.height ) );

    queue.enqueueReadImage( outputImage, CL_TRUE, origin, region,
                            output.info().row_pitch, 0, output.data() );


    return NO_ERROR;
  }
};

up<Stage> IllusionEngine::stage_normalize()
{
  up<NormalizeOpenCLStage> retval = std::make_unique<NormalizeOpenCLStage>();
  retval->context = mData->context;
  retval->kernel = cl::Kernel( mData->program, "normalize_uint16_image" );
  retval->queue = mData->queue;

  return retval;
}

class EqualizeStage : public EqualizeHistoStage
{
public:

  cl::Context context;
  cl::Kernel kernel;
  cl::CommandQueue queue;

  status_t run( Image2D& input, Image2D& output ) {

    if( input.info().format != ImageBufferInfo::PIX_FMT_GRAY_FLOAT ) {
      ALOGE( "input format is not correct: %d", input.info().format );
      return UNKNOWN_ERROR;
    }

    if( output.info().format != ImageBufferInfo::PIX_FMT_GRAY_FLOAT ) {
      ALOGE( "output format is not correct: %d", output.info().format );
      return UNKNOWN_ERROR;
    }

    cl_int err;
    const Dim2D& inputSize = input.info().size;
    const Dim2D& outputSize = output.info().size;

    cl::Image2D inputImage( context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                            cl::ImageFormat( CL_R, CL_FLOAT ), inputSize.width, inputSize.height, 0, input.data(), &err );

    if( err != 0 ) {
      ALOGE( "error creating input buffer: %d", err );
      return UNKNOWN_ERROR;
    }

    cl::Image2D outputImage( context, CL_MEM_WRITE_ONLY,
                             cl::ImageFormat( CL_R, CL_FLOAT ), outputSize.width, outputSize.height, 0 );

    cl::Buffer cdfBuffer( context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof( float ) * cdf.size(), cdf.data(), &err );
    if( err != 0 ) {
      ALOGE( "error creating cdf buffer: %d", err );
      return UNKNOWN_ERROR;
    }

    const std::array<std::size_t, 3> origin = {0, 0, 0};
    const std::array<std::size_t, 3> region = {inputSize.width, inputSize.height, 1};

    const cl_int N = cdf.size();

    if( ( err = kernel.setArg( 0, inputImage ) ) != 0 ) {
      ALOGE( "error setting kernel arg" );
    }
    if( ( err = kernel.setArg( 1, N ) ) != 0 ) {
      ALOGE( "error setting kernel arg" );
    }
    if( ( err = kernel.setArg( 2, cdfBuffer ) ) != 0 ) {
      ALOGE( "error setting kernel arg" );
    }
    if( ( err = kernel.setArg( 3, outputImage ) ) != 0 ) {
      ALOGE( "error setting kernel arg" );
    }

    queue.enqueueNDRangeKernel( kernel, cl::NullRange, cl::NDRange( inputSize.width, inputSize.height ) );

    queue.enqueueReadImage( outputImage, CL_TRUE, origin, region,
                            output.info().row_pitch, 0, output.data() );


    return NO_ERROR;
  }
};

up<EqualizeHistoStage> IllusionEngine::stage_equalize()
{
  up<EqualizeStage> retval = std::make_unique<EqualizeStage>();
  retval->context = mData->context;
  retval->kernel = cl::Kernel( mData->program, "equalizeHisto" );
  retval->queue = mData->queue;

  return retval;
}


class RawEqualizeStage : public EqualizeHistoStage
{
public:

  cl::Context context;
  cl::Kernel kernel;
  cl::CommandQueue queue;

  status_t run( Image2D& input, Image2D& output ) {

    if( input.info().format != ImageBufferInfo::PIX_FMT_GRAY_UINT16 ) {
      ALOGE( "input format is not correct: %d", input.info().format );
      return UNKNOWN_ERROR;
    }

    if( output.info().format != ImageBufferInfo::PIX_FMT_GRAY_FLOAT ) {
      ALOGE( "output format is not correct: %d", output.info().format );
      return UNKNOWN_ERROR;
    }

    cl_int err;
    const Dim2D& inputSize = input.info().size;
    const Dim2D& outputSize = output.info().size;

    cl::Image2D inputImage( context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                            cl::ImageFormat( CL_R, CL_SIGNED_INT16 ), inputSize.width, inputSize.height, 0, input.data(), &err );

    if( err != 0 ) {
      ALOGE( "error creating input buffer: %d", err );
      return UNKNOWN_ERROR;
    }

    cl::Image2D outputImage( context, CL_MEM_WRITE_ONLY,
                             cl::ImageFormat( CL_R, CL_FLOAT ), outputSize.width, outputSize.height, 0 );

    cl::Buffer cdfBuffer( context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof( float ) * cdf.size(), cdf.data(), &err );
    if( err != 0 ) {
      ALOGE( "error creating cdf buffer: %d", err );
      return UNKNOWN_ERROR;
    }

    const std::array<std::size_t, 3> origin = {0, 0, 0};
    const std::array<std::size_t, 3> region = {inputSize.width, inputSize.height, 1};

    const cl_int N = cdf.size();

    if( ( err = kernel.setArg( 0, inputImage ) ) != 0 ) {
      ALOGE( "error setting kernel arg" );
    }
    if( ( err = kernel.setArg( 1, N ) ) != 0 ) {
      ALOGE( "error setting kernel arg" );
    }
    if( ( err = kernel.setArg( 2, cdfBuffer ) ) != 0 ) {
      ALOGE( "error setting kernel arg" );
    }
    if( ( err = kernel.setArg( 3, outputImage ) ) != 0 ) {
      ALOGE( "error setting kernel arg" );
    }

    queue.enqueueNDRangeKernel( kernel, cl::NullRange, cl::NDRange( inputSize.width, inputSize.height ) );

    queue.enqueueReadImage( outputImage, CL_TRUE, origin, region,
                            output.info().row_pitch, 0, output.data() );


    return NO_ERROR;
  }
};

up<EqualizeHistoStage> IllusionEngine::stage_raw_equalize()
{
  up<RawEqualizeStage> retval = std::make_unique<RawEqualizeStage>();
  retval->context = mData->context;
  retval->kernel = cl::Kernel( mData->program, "equalizeRawHisto" );
  retval->queue = mData->queue;

  return retval;
}

} // namespace videray

