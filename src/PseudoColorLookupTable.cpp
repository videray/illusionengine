
#include <illusionengine/PseudoColorLookupTable.h>

#include <cmath>

#ifndef PI
  #define PI 3.14159265359
#endif



namespace videray {

static
void setColor( Image1D& image, size_t i, const Color& c )
{
  uint16_t* pixels = reinterpret_cast<uint16_t*>( image.data() );
  pixels[i] = RGB565::fromColor( c );
}

void PseudoColorLookupTable::createGrayLookupTable( int resolution, Image1D& lut )
{
  lut = Image1D( ImageBufferInfo::PIX_FMT_RGB565, Dim1D( resolution ) );
  for( int i = 0; i < resolution; i++ ) {
    Color c;
    float intensity = ( float )i / resolution;
    c.r = intensity;
    c.g = intensity;
    c.b = intensity;
    c.a = 1.0f;

    setColor( lut, i, c );
  }
}

void PseudoColorLookupTable::createSpectrumLookupTable( int resolution, Image1D& lut )
{
  lut = Image1D( ImageBufferInfo::PIX_FMT_RGB565, Dim1D( resolution ) );
  for( int i = 0; i < resolution; i++ ) {
    Color c;
    float hue = ( float )360 * i / resolution;
    hue += 180.0f;
    hue = fmod( hue, 360.0f );

    Color::HSVtoRGB( c.r, c.g, c.b, hue, 1, 1 );
    c.a = 1.0f;

    setColor( lut, i, c );
  }
}

void PseudoColorLookupTable::createColorBlendLUT( int resolution, std::vector<Color> colors, Image1D& lut )
{
  lut = Image1D( ImageBufferInfo::PIX_FMT_RGB565, Dim1D( resolution ) );
  const float shades = ( float ) resolution / ( colors.size() - 1 );

  for( int i = 0; i < resolution; i++ ) {

    float x = ( float ) i / shades;
    int leftColor = ( int ) x;
    float t = x - floor( x );

    Color a = colors[leftColor];
    Color b = colors[leftColor + 1];
    setColor( lut, i, Color::mix( a, b, t ) );
  }
}

void PseudoColorLookupTable::createThermalLUT( int resolution, Image1D& lut )
{
  std::vector<Color> colors;
  colors.push_back( Color::hexToColor( "000000" ) );
  colors.push_back( Color::hexToColor( "0000FF" ) );
  colors.push_back( Color::hexToColor( "00FFFF" ) );
  colors.push_back( Color::hexToColor( "FFFFFF" ) );
  colors.push_back( Color::hexToColor( "00FF00" ) );
  colors.push_back( Color::hexToColor( "FFFF00" ) );
  colors.push_back( Color::hexToColor( "FF0000" ) );
  createColorBlendLUT( resolution, colors, lut );
}

void PseudoColorLookupTable::createRedHotLUT( int resolution, Image1D& lut )
{
  std::vector<Color> colors;
  colors.push_back( Color::hexToColor( "000000" ) );
  colors.push_back( Color::hexToColor( "FF0000" ) );
  colors.push_back( Color::hexToColor( "FFFF00" ) );
  colors.push_back( Color::hexToColor( "FFFFFF" ) );
  createColorBlendLUT( resolution, colors, lut );
}

void PseudoColorLookupTable::createSRangeLUT( int resolution, Image1D& lut )
{
  std::vector<Color> colors;
  colors.push_back( Color::hexToColor( "000000" ) );
  colors.push_back( Color::hexToColor( "0000FF" ) );

  colors.push_back( Color::hexToColor( "000000" ) );
  colors.push_back( Color::hexToColor( "00FF00" ) );

  colors.push_back( Color::hexToColor( "000000" ) );
  colors.push_back( Color::hexToColor( "FFFF00" ) );

  colors.push_back( Color::hexToColor( "000000" ) );
  colors.push_back( Color::hexToColor( "FF0000" ) );

  colors.push_back( Color::hexToColor( "000000" ) );
  colors.push_back( Color::hexToColor( "FFFFFF" ) );

  createColorBlendLUT( resolution, colors, lut );
}


void PseudoColorLookupTable::create8Color( int resolution, Image1D& lut )
{
  std::vector<Color> colors;
  colors.push_back( Color::hexToColor( "000000" ) );
  colors.push_back( Color::hexToColor( "FF0000" ) );
  colors.push_back( Color::hexToColor( "00FF00" ) );
  colors.push_back( Color::hexToColor( "0000FF" ) );
  colors.push_back( Color::hexToColor( "00FFFF" ) );
  colors.push_back( Color::hexToColor( "FF00FF" ) );
  colors.push_back( Color::hexToColor( "FFFF00" ) );
  colors.push_back( Color::hexToColor( "FFFFFF" ) );


  lut = Image1D( ImageBufferInfo::PIX_FMT_RGB565, Dim1D( resolution ) );
  const float shades = ( float ) resolution / ( colors.size() - 1 );

  for( int i = 0; i < resolution; i++ ) {

    float x = ( float ) i / shades;
    int leftColor = ( int ) x;

    setColor( lut, i, colors[leftColor] );
  }
}


void PseudoColorLookupTable::create16Color( int resolution, Image1D& lut )
{
  std::vector<Color> colors;
  colors.push_back( Color::hexToColor( "000000" ) );
  colors.push_back( Color::hexToColor( "000AA0" ) );
  colors.push_back( Color::hexToColor( "0A0CE7" ) );
  colors.push_back( Color::hexToColor( "0D6DFA" ) );
  colors.push_back( Color::hexToColor( "15ABF5" ) );
  colors.push_back( Color::hexToColor( "1EDDFA" ) );
  colors.push_back( Color::hexToColor( "25FC29" ) );
  colors.push_back( Color::hexToColor( "CBF62E" ) );
  colors.push_back( Color::hexToColor( "FFFF33" ) );
  colors.push_back( Color::hexToColor( "FEE030" ) );
  colors.push_back( Color::hexToColor( "FE8A29" ) );
  colors.push_back( Color::hexToColor( "F95F27" ) );
  colors.push_back( Color::hexToColor( "F52325" ) );
  colors.push_back( Color::hexToColor( "F623C4" ) );
  colors.push_back( Color::hexToColor( "E2B1D2" ) );
  colors.push_back( Color::hexToColor( "FFFFFF" ) );


  lut = Image1D( ImageBufferInfo::PIX_FMT_RGB565, Dim1D( resolution ) );
  const float shades = ( float ) resolution / ( colors.size() - 1 );

  for( int i = 0; i < resolution; i++ ) {

    float x = ( float ) i / shades;
    int leftColor = ( int ) x;

    setColor( lut, i, colors[leftColor] );
  }
}

float sinFun( float frequency, float phase, float x )
{
  float retval = cos( 2.0 * PI * frequency * x + PI * phase ) * 0.5 + 0.5;
  return retval;
}

void PseudoColorLookupTable::createSin( int resolution, Image1D& lut, float freq, float r, float g, float b )
{
  lut = Image1D( ImageBufferInfo::PIX_FMT_RGB565, Dim1D( resolution ) );
  for( int i = 0; i < resolution; i++ ) {
    Color c;
    float x = ( float )i / resolution;
    c.r = sinFun( freq, r, x );
    c.g = sinFun( freq, g, x );
    c.b = sinFun( freq, b, x );

    setColor( lut, i, c );
  }
}

} // namespace videray