

#include <illusionengine/Color.h>

#include <videray/core.h>

#include <stdint.h>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <cstdlib>


namespace videray {

Color Color::hexToColor( const char* hexStr )
{
  Color retval;
  int len = strlen( hexStr );
  if( len == 6 ) {
    uint32_t value = strtoul( hexStr, nullptr, 16 );
    retval.r = ( ( 0xFF ) & ( value >> 16 ) ) / 255.0f;
    retval.g = ( ( 0xFF ) & ( value >>  8 ) ) / 255.0f;
    retval.b = ( ( 0xFF ) & ( value >>  0 ) ) / 255.0f;
    retval.a = 1.0f;
  }
  return retval;
}

void Color::HSVtoRGB( float& fR, float& fG, float& fB, const float& fH, const float& fS, const float& fV )
{
  float fC = fV * fS; // Chroma
  float fHPrime = fmod( fH / 60.0, 6 );
  float fX = fC * ( 1 - fabs( fmod( fHPrime, 2 ) - 1 ) );
  float fM = fV - fC;

  if( 0 <= fHPrime && fHPrime < 1 ) {
    fR = fC;
    fG = fX;
    fB = 0;
  } else if( 1 <= fHPrime && fHPrime < 2 ) {
    fR = fX;
    fG = fC;
    fB = 0;
  } else if( 2 <= fHPrime && fHPrime < 3 ) {
    fR = 0;
    fG = fC;
    fB = fX;
  } else if( 3 <= fHPrime && fHPrime < 4 ) {
    fR = 0;
    fG = fX;
    fB = fC;
  } else if( 4 <= fHPrime && fHPrime < 5 ) {
    fR = fX;
    fG = 0;
    fB = fC;
  } else if( 5 <= fHPrime && fHPrime < 6 ) {
    fR = fC;
    fG = 0;
    fB = fX;
  } else {
    fR = 0;
    fG = 0;
    fB = 0;
  }

  fR += fM;
  fG += fM;
  fB += fM;
}

void Color::RGBtoHSV( const float& fR, const float& fG, const float& fB, float& fH, float& fS, float& fV )
{
  float fCMax = MAX( MAX( fR, fG ), fB );
  float fCMin = MIN( MIN( fR, fG ), fB );
  float fDelta = fCMax - fCMin;

  if( fDelta > 0 ) {
    if( fCMax == fR ) {
      fH = 60 * ( fmod( ( ( fG - fB ) / fDelta ), 6 ) );
    } else if( fCMax == fG ) {
      fH = 60 * ( ( ( fB - fR ) / fDelta ) + 2 );
    } else if( fCMax == fB ) {
      fH = 60 * ( ( ( fR - fG ) / fDelta ) + 4 );
    }

    if( fCMax > 0 ) {
      fS = fDelta / fCMax;
    } else {
      fS = 0;
    }

    fV = fCMax;
  } else {
    fH = 0;
    fS = 0;
    fV = fCMax;
  }

  if( fH < 0 ) {
    fH = 360 + fH;
  }
}

Color Color::mix( const Color& a, const Color& b, float t )
{
  Color retval;
  retval.r = a.r + ( b.r - a.r ) * t;
  retval.g = a.g + ( b.g - a.g ) * t;
  retval.b = a.b + ( b.b - a.b ) * t;
  retval.a = a.a + ( b.a - a.a ) * t;

  return retval;
}

Color RGB565::toColor( uint16_t input )
{
  Color retval;
  retval.r = ( float )( 0x1F & ( input >> 11 ) ) / 31;
  retval.g = ( float )( 0x3F & ( input >> 5 ) ) / 63;
  retval.b = ( float )( 0x1F & ( input >> 0 ) ) / 31;
  retval.a = 1.0f;

  return retval;
}

uint16_t RGB565::fromColor( Color input )
{
  uint16_t retval;

  retval = 0;
  retval |= ( ( ( uint16_t )( input.r * 31 ) ) << 11 );
  retval |= ( ( ( uint16_t )( input.g * 63 ) ) << 5 );
  retval |= ( ( ( uint16_t )( input.b * 31 ) ) << 0 );

  return retval;
}


}