
#include <illusionengine/IllusionEngine.h>
#include <baseline/Log.h>

#include "opencl/CLContext.h"



using namespace videray;


static void gAndroidLog( const char* format, va_list args )
{
  __android_log_vprint( ANDROID_LOG_ERROR, "LIBILLUSION", format, args );
}

int main( int argc, char* argv[] )
{

  baseline::gLogFunction = gAndroidLog;

  cl_platform_id platforms[5];
  cl_uint num_platforms;
  cl_int ret;
  size_t value_size;
  char str[32];

  ret = clGetPlatformIDs( 5, platforms, &num_platforms );

  CLPlatform platform( platforms[0] );
  platform.getInfo();

  Vector<cl_device_id> devices;
  platform.getDevices( CL_DEVICE_TYPE_ALL, devices );
  CLDevice deviceId( devices[0] );

  const cl_device_type type = deviceId.deviceType();
  bool isGPU = type == CL_DEVICE_TYPE_GPU;

  CLContext ctx;
  ctx.init( deviceId.deviceId() );

  printf( "done \n" );
}