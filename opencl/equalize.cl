R"~~(

__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;
__constant sampler_t colorSampler = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

__constant float NORM_UINT16 = 1.0f / (0xFFFF-1);

__kernel void normalize_uint16_image(__read_only image2d_t in, __write_only image2d_t out)
{
    int x = get_global_id(0);
    int y = get_global_id(1);

    const int2 coord = {x, y};
    ushort rawIntensity = as_ushort( read_imagei(in, sampler, coord).r );
    float intensity = clamp( rawIntensity * NORM_UINT16, 0.0f, 1.0f);
    float4 color = {intensity, 0, 0, 0};
    write_imagef(out, coord, color);
}

__kernel void calcHisto(__read_only image2d_t in, const int N, __global uint* histo)
{
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    float intensity = read_imagef(in, sampler, coord).r;
    int bin = (int)((N-1) * intensity);
    atomic_inc(&histo[bin]);
}

__kernel void equalizeHisto(__read_only image2d_t in, const int N, __global const float* cdf, __write_only image2d_t out)
{
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    float intensity = read_imagef(in, sampler, coord).r;
    int bin = (int)((N-1) * intensity);
    intensity = cdf[bin];
    float4 color = (float4)(intensity, 0, 0, 0);
    write_imagef(out, coord, color);
}

__kernel void equalizeRawHisto(__read_only image2d_t in, const int N, __global const float* cdf, __write_only image2d_t out)
{
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    ushort rawIntensity = as_ushort( read_imagei(in, sampler, coord).r );
    float intensity = clamp( rawIntensity * NORM_UINT16, 0.0f, 1.0f);
    int bin = (int)((N-1) * intensity);
    intensity = cdf[bin];
    float4 color = (float4)(intensity, 0, 0, 0);
    write_imagef(out, coord, color);
}

__kernel void psudoColorize(__read_only image2d_t in, __read_only image1d_t colorLUT, __write_only image2d_t out)
{
    int x = get_global_id(0);
    int y = get_global_id(1);

    const int2 coord = {x, y};
    float intensity = read_imagef(in, sampler, coord).r;

    float4 color = read_imagef(colorLUT, colorSampler, intensity);
    write_imagef(out, coord, color);
}

)~~"