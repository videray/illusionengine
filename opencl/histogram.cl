R"~~(

__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;
__constant float scale = 1.0f / 0xFFFF;

__kernel void partialHistogramX(__read_only image2d_t in, const int N, __global int* partHist)
{
    int y = get_global_id(0);
    const int width = get_image_width(in);

    for(int i=0;i<N;i++) {
        const size_t id = y * N + i;
        partHist[id] = 0;
    }
    
    for(int x = 0;x<width;x++) {
        int2 coord = {x, y};
        ushort rawIntensity = as_ushort( read_imagei(in, sampler, coord).r );
        float intensity = clamp(rawIntensity * scale, 0.0f, 1.0f);
        int bin = (int)((N - 1) * intensity);
        size_t id = y * N + bin;
        partHist[id]++;
    }
}

)~~"