#include <gtest/gtest.h>

#include <illusionengine/ImagePipeline.h>

// #include <CL/cl.hpp>

#define CL_HPP_MINIMUM_OPENCL_VERSION 100
#define CL_HPP_TARGET_OPENCL_VERSION 120
#include <cl2.hpp>


#include <vector>

using namespace std;
using namespace videray;

const std::string kernel1 =
#include "histogram.cl"
  ;

const std::string kernel2 =
#include "equalize.cl"
  ;

TEST( OpenCLTest, calcHisto )
{
  std::vector<cl::Platform> platforms;
  cl::Platform::get( &platforms );
  ASSERT_TRUE( platforms.size() > 0 );

  cl::Platform platform = cl::Platform::setDefault( platforms.front() );

  std::vector<cl::Device> devices;
  platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );

  ASSERT_TRUE( devices.size() > 0 );


  cl::Device device = cl::Device::setDefault( devices.front() );
  cl::Context context = cl::Context::setDefault( cl::Context( device ) );
  cl::CommandQueue queue = cl::CommandQueue::setDefault( cl::CommandQueue( context, device ) );

  std::vector<std::string> programSrc {kernel2};

  cl::Program myProgram( programSrc );
  cl_int err = myProgram.build( "-cl-std=CL1.2" );
  if( err != 0 ) {
    std::string buildlog = myProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG>().front().second;
    std::cout << buildlog << std::endl;
  }
  ASSERT_EQ( 0, err );

  float* imageBuf = ( float* )alloca( sizeof( float ) * 25 );
  for( int i = 0; i < 25; i++ ) {
    imageBuf[i] = 0.0f;
  }

  imageBuf[5] = 0.3f;

  cl::Image2D input( context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                     cl::ImageFormat( CL_R, CL_FLOAT ), 5, 5, 0, imageBuf, &err );

  ASSERT_EQ( 0, err );

  const cl_int N = 10;

  cl_uint* histo = ( cl_uint* ) alloca( sizeof( cl_uint ) * N );
  memset( histo, 0, sizeof( cl_uint ) * N );

  cl::Buffer histoBuf( context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof( cl_uint ) * N, histo );


  cl::Kernel kernel( myProgram, "calcHisto" );
  kernel.setArg( 0, input );
  kernel.setArg( 1, N );
  kernel.setArg( 2, histoBuf );
  queue.enqueueNDRangeKernel( kernel, cl::NullRange, cl::NDRange( 5, 5 ) );
  queue.enqueueReadBuffer( histoBuf, CL_TRUE, 0, sizeof( cl_uint ) * N, histo );

  for( int i = 0; i < N; i++ ) {
    //EXPECT_EQ(0, histo[i]);
    std::cout << i << " : " << histo[i] << std::endl;
  }

  EXPECT_EQ( 24, histo[0] );
  EXPECT_EQ( 1, histo[2] );
}

TEST( OpenCLTest, normalize_uint16 )
{
  std::vector<cl::Platform> platforms;
  cl::Platform::get( &platforms );
  ASSERT_TRUE( platforms.size() > 0 );

  cl::Platform platform = cl::Platform::setDefault( platforms.front() );

  std::vector<cl::Device> devices;
  platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );

  ASSERT_TRUE( devices.size() > 0 );


  cl::Device device = cl::Device::setDefault( devices.front() );
  cl::Context context = cl::Context::setDefault( cl::Context( device ) );
  cl::CommandQueue queue = cl::CommandQueue::setDefault( cl::CommandQueue( context, device ) );

  std::vector<std::string> programSrc {kernel2};

  cl::Program myProgram( programSrc );
  cl_int err = myProgram.build( "-cl-std=CL1.2" );
  if( err != 0 ) {
    std::string buildlog = myProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG>().front().second;
    std::cout << buildlog << std::endl;
  }
  ASSERT_EQ( 0, err );

  uint16_t* imageBuf = ( uint16_t* )alloca( sizeof( uint16_t ) * 25 );
  for( int i = 0; i < 25; i++ ) {
    imageBuf[i] = 0;
  }

  imageBuf[0] = 0xFFFF;
  imageBuf[1] = 0x7FFF;
  imageBuf[2] = 0x3FFF;
  imageBuf[3] = 0x1FFF;
  imageBuf[4] = 0x0FFF;
  imageBuf[5] = 0x07FF;
  imageBuf[6] = 0x03FF;
  imageBuf[7] = 0x01FF;
  imageBuf[8] = 0x00FF;
  imageBuf[9] = 0x007F;
  imageBuf[10] = 0x003F;
  imageBuf[11] = 0x001F;
  imageBuf[12] = 0x000F;
  imageBuf[13] = 0x0007;
  imageBuf[14] = 0x0003;
  imageBuf[15] = 0x0001;


  cl::Image2D input( context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                     cl::ImageFormat( CL_R, CL_SIGNED_INT16 ), 5, 5, 0, imageBuf, &err );

  ASSERT_EQ( 0, err );

  cl::Image2D output( context, CL_MEM_READ_WRITE, cl::ImageFormat( CL_R, CL_FLOAT ), 5, 5 );

  float* outputData = ( float* ) alloca( sizeof( float ) * 5 * 5 );

  cl::Kernel kernel( myProgram, "normalize_uint16_image" );
  kernel.setArg( 0, input );
  kernel.setArg( 1, output );
  queue.enqueueNDRangeKernel( kernel, cl::NullRange, cl::NDRange( 5, 5 ) );
  queue.enqueueReadImage( output, CL_TRUE, {0, 0, 0}, {5, 5, 1}, 0, 0, outputData );


  for( int i = 0; i < 25; i++ ) {
    std::cout << i << " : " << outputData[i] << std::endl;
  }

  EXPECT_NEAR( 1.0f, outputData[0], 0.000015259f );
  EXPECT_NEAR( 0.5f, outputData[1], 0.000015259f );
  EXPECT_NEAR( 0.25f, outputData[2], 0.000015259f );
  EXPECT_NEAR( 0.125f, outputData[3], 0.000015259f );
  EXPECT_NEAR( 0.0625f, outputData[4], 0.000015259f );
  EXPECT_NEAR( 0.03125f, outputData[5], 0.000015259f );
  EXPECT_NEAR( 0.015625f, outputData[6], 0.000015259f );
  EXPECT_NEAR( 0.0078125f, outputData[7], 0.000015259f );
  EXPECT_NEAR( 0.00390625f, outputData[8], 0.000015259f );
  EXPECT_NEAR( 0.001953125f, outputData[9], 0.000015259f );
  EXPECT_NEAR( 0.000976562f, outputData[10], 0.000015259f );
  EXPECT_NEAR( 0.000488281f, outputData[11], 0.000015259f );
  EXPECT_NEAR( 0.000244141f, outputData[12], 0.000015259f );
  EXPECT_NEAR( 0.00012207f, outputData[13], 0.000015259f );
  EXPECT_NEAR( 0.000061035f, outputData[14], 0.000015259f );
  EXPECT_NEAR( 0.000030518f, outputData[15], 0.000015259f );

}

TEST( OpenCLTest, program )
{
  std::vector<cl::Platform> platforms;
  cl::Platform::get( &platforms );
  ASSERT_TRUE( platforms.size() > 0 );

  cl::Platform platform = cl::Platform::setDefault( platforms.front() );

  std::vector<cl::Device> devices;
  platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );

  ASSERT_TRUE( devices.size() > 0 );


  cl::Device device = cl::Device::setDefault( devices.front() );
  cl::Context context = cl::Context::setDefault( cl::Context( device ) );
  cl::CommandQueue queue = cl::CommandQueue::setDefault( cl::CommandQueue( context, device ) );

  vector<cl::ImageFormat> supportedImageFormats;
  context.getSupportedImageFormats( CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, CL_MEM_OBJECT_IMAGE2D, &supportedImageFormats );

  for( const cl::ImageFormat& f : supportedImageFormats ) {
    printf( "f = 0x%x 0x%x\n", f.image_channel_order, f.image_channel_data_type );
  }

  std::vector<std::string> programSrc {kernel1};

  cl::Program myProgram( programSrc );
  cl_int err = myProgram.build( "-cl-std=CL1.2" );
  if( err != 0 ) {
    std::string buildlog = myProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG>().front().second;
    std::cout << buildlog << std::endl;
  }
  ASSERT_EQ( 0, err );

  uint16_t* imageBuf = ( uint16_t* )alloca( sizeof( uint16_t ) * 30 );
  for( int i = 0; i < 30; i++ ) {
    imageBuf[i] = 0;
  }

  imageBuf[20] = 44000;
  imageBuf[21] = 0xFFFF;


  cl::Image2D image( context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                     cl::ImageFormat( CL_R, CL_SIGNED_INT16 ), 15, 2, 0, imageBuf, &err );

  ASSERT_EQ( 0, err );
  ASSERT_EQ( 2, image.getImageInfo<CL_IMAGE_HEIGHT>() );

  const cl_int N = 10;

  cl::Buffer partHistoBuf( context, CL_MEM_READ_WRITE, sizeof( cl_int ) * N * image.getImageInfo<CL_IMAGE_HEIGHT>() );

  cl::Kernel kernel( myProgram, "partialHistogramX" );
  kernel.setArg( 0, image );
  kernel.setArg( 1, N );
  kernel.setArg( 2, partHistoBuf );
  queue.enqueueNDRangeKernel( kernel, cl::NullRange, cl::NDRange( image.getImageInfo<CL_IMAGE_HEIGHT>() ) );

  cl_int* partialHisto = ( cl_int* ) alloca( sizeof( cl_int ) * N * image.getImageInfo<CL_IMAGE_HEIGHT>() );
  queue.enqueueReadBuffer( partHistoBuf, CL_TRUE, 0, sizeof( cl_int ) * N * image.getImageInfo<CL_IMAGE_HEIGHT>(), partialHisto );

  //EXPECT_EQ(0, partialHisto[0]);

  for( int i = 0; i < N * image.getImageInfo<CL_IMAGE_HEIGHT>(); i++ ) {
    std::cout << i << " : " << partialHisto[i] << std::endl;
  }

  EXPECT_EQ( 15, partialHisto[0] );
  EXPECT_EQ( 13, partialHisto[N] );
  EXPECT_EQ( 1, partialHisto[19] );


}
