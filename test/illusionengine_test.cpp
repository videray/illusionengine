#include <gtest/gtest.h>

#include <illusionengine/IllusionEngine.h>
#include <illusionengine/Image2D.h>
#include <illusionengine/ImagePipeline.h>

using namespace videray;
using namespace android;

TEST( IllusionEngine, NormalizeUin16Stage )
{
  IllusionEngine& engine = IllusionEngine::getInstance();

  Image2D input( ImageBufferInfo::PIX_FMT_GRAY_UINT16, Dim2D( 5, 5 ) );
  Image2D output( ImageBufferInfo::PIX_FMT_GRAY_FLOAT, Dim2D( 5, 5 ) );

  uint16_t* imageBuf = reinterpret_cast<uint16_t*>( input.data() );
  imageBuf[0] = 0xFFFF;
  imageBuf[1] = 0x7FFF;
  imageBuf[2] = 0x3FFF;
  imageBuf[3] = 0x1FFF;
  imageBuf[4] = 0x0FFF;
  imageBuf[5] = 0x07FF;
  imageBuf[6] = 0x03FF;
  imageBuf[7] = 0x01FF;
  imageBuf[8] = 0x00FF;
  imageBuf[9] = 0x007F;
  imageBuf[10] = 0x003F;
  imageBuf[11] = 0x001F;
  imageBuf[12] = 0x000F;
  imageBuf[13] = 0x0007;
  imageBuf[14] = 0x0003;
  imageBuf[15] = 0x0001;

  ASSERT_EQ( NO_ERROR, engine.stage_normalize()->run( input, output ) );

  const float* outputData = reinterpret_cast<const float*>( output.data() );

  EXPECT_NEAR( 1.0f, outputData[0], 0.000015259f );
  EXPECT_NEAR( 0.5f, outputData[1], 0.000015259f );
  EXPECT_NEAR( 0.25f, outputData[2], 0.000015259f );
  EXPECT_NEAR( 0.125f, outputData[3], 0.000015259f );
  EXPECT_NEAR( 0.0625f, outputData[4], 0.000015259f );
  EXPECT_NEAR( 0.03125f, outputData[5], 0.000015259f );
  EXPECT_NEAR( 0.015625f, outputData[6], 0.000015259f );
  EXPECT_NEAR( 0.0078125f, outputData[7], 0.000015259f );
  EXPECT_NEAR( 0.00390625f, outputData[8], 0.000015259f );
  EXPECT_NEAR( 0.001953125f, outputData[9], 0.000015259f );
  EXPECT_NEAR( 0.000976562f, outputData[10], 0.000015259f );
  EXPECT_NEAR( 0.000488281f, outputData[11], 0.000015259f );
  EXPECT_NEAR( 0.000244141f, outputData[12], 0.000015259f );
  EXPECT_NEAR( 0.00012207f, outputData[13], 0.000015259f );
  EXPECT_NEAR( 0.000061035f, outputData[14], 0.000015259f );
  EXPECT_NEAR( 0.000030518f, outputData[15], 0.000015259f );
}

TEST( IllusionEngine, EqualizeHistoStage )
{
  IllusionEngine& engine = IllusionEngine::getInstance();

  Image2D input( ImageBufferInfo::PIX_FMT_GRAY_FLOAT, Dim2D( 5, 5 ) );
  Image2D output( ImageBufferInfo::PIX_FMT_GRAY_FLOAT, Dim2D( 5, 5 ) );

  {
    float* buf = reinterpret_cast<float*>( input.data() );
    for( int i = 0; i < 25; i++ ) {
      buf[i] = 0.5f;
    }
    buf[3] = 0.25f;
    buf[8] = 0.73f;
  }

  up<EqualizeHistoStage> stage = engine.stage_equalize();
  stage->cdf.resize( 10 );
  stage->cdf[0] = 0.0f / 25;
  stage->cdf[1] = 0.0f / 25;
  stage->cdf[2] = 0.0f / 25;
  stage->cdf[3] = 1.0f / 25;
  stage->cdf[4] = 1.0f / 25;
  stage->cdf[5] = 24.0f / 25;
  stage->cdf[6] = 24.0f / 25;
  stage->cdf[7] = 24.0f / 25;
  stage->cdf[8] = 25.0f / 25;
  stage->cdf[9] = 25.0f / 25;

  ASSERT_EQ( NO_ERROR, stage->run( input, output ) );

  const float* outputData = reinterpret_cast<const float*>( output.data() );

  for( int i = 0; i < 25; i++ ) {
    std::cout << i << " : " << outputData[i] << std::endl;
  }

  EXPECT_FLOAT_EQ( 0.04f, outputData[0] );
  EXPECT_FLOAT_EQ( 0.04f, outputData[1] );
  EXPECT_FLOAT_EQ( 0.04f, outputData[2] );
  EXPECT_FLOAT_EQ( 0.00f, outputData[3] );
  EXPECT_FLOAT_EQ( 0.04f, outputData[4] );
  EXPECT_FLOAT_EQ( 0.96f, outputData[8] );
  EXPECT_TRUE( outputData[8] > outputData[3] );

}