#include <gtest/gtest.h>

int main( int argc, char* argv[] )
{
  // Run the tests
  testing::InitGoogleTest( &argc, argv );
  int ret = RUN_ALL_TESTS();

  return ret;
}